#!/bin/sh
#
cpt=0
#Create a destination dir called pgmDir
pgmDir=PGM16
mkdir $pgmDir

for i in *.DNG ; do # for NEF images
    echo "Image $cpt ($i)"
    #Raw Conversion
    dcraw -v -k 0 -4 $i
done
mv *.pgm $pgmDir
